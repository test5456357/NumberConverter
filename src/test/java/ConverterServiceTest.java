import org.example.ConvertException;
import org.example.ConverterService;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ConverterServiceTest {
    @Test
    public void testSuccess() throws ConvertException, NumberFormatException {
        assertEquals(1000d, ConverterService.numberConverter("1000"));
        assertEquals(234235.23d, ConverterService.numberConverter("$234.235,23"));
        assertEquals(234235.23d, ConverterService.numberConverter("$ 234 235,23"));
        assertEquals(54.23d, ConverterService.numberConverter("54.23$"));
        assertEquals(-235.3d, ConverterService.numberConverter("235.3-"));
        assertEquals(-5d, ConverterService.numberConverter(" $5-"));
        assertEquals(-23234234.02d, ConverterService.numberConverter("-$23,234,234.02"));
        assertEquals(-6.23d, ConverterService.numberConverter("$-6.23"));
        assertEquals(-6.2d, ConverterService.numberConverter("6.2$-"));
        assertEquals(-53234.0d, ConverterService.numberConverter("53,234.0-$"));
        assertEquals(-23d, ConverterService.numberConverter("-23$"));
    }

    @Test
    public void testIncorrect() {
        assertThrows(ConvertException.class, () -> ConverterService.numberConverter("$23d34.34"));
        assertThrows(ConvertException.class, () -> ConverterService.numberConverter("invalidInput"));
        assertThrows(ConvertException.class, () -> ConverterService.numberConverter(null));
        assertThrows(ConvertException.class, () -> ConverterService.numberConverter("+53,34.0-$"));
        assertThrows(ConvertException.class, () -> ConverterService.numberConverter("-1,064.02-$"));
        assertThrows(ConvertException.class, () -> ConverterService.numberConverter("$3.01$-"));
        assertThrows(ConvertException.class, () -> ConverterService.numberConverter("*3.01-"));
    }

}
