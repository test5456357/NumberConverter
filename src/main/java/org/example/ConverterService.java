package org.example;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The ConverterService class provides a static method for converting string numerical values from Excel to Double type.
 */
public class ConverterService {

    static List<Character> currencies = List.of('$','€');
    static Pattern separatePattern = Pattern.compile("^(\\D*)(\\d?.*\\d)(\\D*)$");
    static Pattern pointPattern = Pattern.compile("^(\\d{1,3}(,?\\d{3})*(\\.\\d+)?)$");
    static Pattern commaPattern = Pattern.compile("^(\\d{1,3}(\\.?\\d{3})*(,\\d+)?)$");
    static Pattern labelPattern = Pattern.compile("^(\\d{1,3}([.,\\s]?\\d{3})*([.,]\\d+)?)$");

    /**
     * Converts a string to a Double.
     * <p>
     * This method attempts to convert a string into a Double. The method logs the start and end of the parsing process.
     * </p>
     *
     * @param source The string to be converted to a Double.
     * @return The Double value represented by the string argument.
     * @throws ConvertException if the provided string is null.
     * @throws NumberFormatException if the string does not contain a parsable Double.
     */
    public static Double numberConverter(String source) throws ConvertException, NumberFormatException  {
        Logger logger = Logger.getLogger(ConverterService.class.getName());
        String logitem = String.format("Started parsing string \"%s\"", source);
        logger.info(logitem);
        Double resutl;
        if (source == null) {
            throw ConvertException.nullValue();
        }

        String preparedSource = source;
        Matcher matcher = separatePattern.matcher(preparedSource);

        // Split into prefix, body and postfix
        String beforeFirstDigit;  // Characters up to the first digit
        String afterLastDigit;  // Characters after the last digit
        if (matcher.find()) {
            beforeFirstDigit = matcher.group(1);  // Characters up to the first digit
            preparedSource = matcher.group(2);  // Characters from first to last digit
            afterLastDigit = matcher.group(3);  // Characters after the last digit
            logger.info("Before first digit: " + beforeFirstDigit);
            logger.info("From first to last digit: " + preparedSource);
            logger.info("After last digit: " + afterLastDigit);
        } else {
            throw ConvertException.numbersNotFound();
        }

        // Spaces are redundant
        Matcher labelMatcher = labelPattern.matcher(preparedSource);
        if (!labelMatcher.matches()) {
            throw ConvertException.thousandSeparatorsIncorrect();
        }
        preparedSource = preparedSource.replaceAll(" ", "");

        // Check for extra characters before and after numbers. Let's remember the sign.
        String currencyAndNegative = beforeFirstDigit + afterLastDigit;
        currencyAndNegative = currencyAndNegative.replace(" ", "");
        long currencyIn = currencyAndNegative.chars().filter(ch -> currencies.contains((char)ch)).count();
        long negativeIn = currencyAndNegative.chars().filter(ch -> ch == '-').count();
        long positiveIn = currencyAndNegative.chars().filter(ch -> ch == '+').count();

        // Extra currency symbols.
        int signFactor = 1;
        if (currencyIn > 1) throw ConvertException.extraCurrencySymbols();
        if (negativeIn + positiveIn > 1) throw ConvertException.extraNegativitySymbols();
        long otherSymbols = currencyAndNegative.length() - currencyIn - negativeIn - positiveIn;
        if (otherSymbols > 0) throw ConvertException.containsInvalidCharacters();
        if (negativeIn > 0) signFactor = -1;


        preparedSource = normalizeSeparator(preparedSource);

        resutl = Double.parseDouble(preparedSource) * signFactor;
        logitem = String.format("Parsing completed successfully. Result: \"%f\"", resutl);
        logger.info(logitem);
        return resutl;
    }

    private static String normalizeSeparator(String str) throws ConvertException{
        String result = str;
        Matcher pointMatcher = pointPattern.matcher(str);
        Matcher commaMatcher = commaPattern.matcher(str);
        if (commaMatcher.matches() && pointMatcher.matches()) {
            boolean haveChar = str.chars().anyMatch(m -> m == '.' || m == ',');
            if (haveChar){
                throw ConvertException.unableToDetermineDelimiter();
            }
        } else if (commaMatcher.matches()) {
            result = result.replaceAll(",", "-");
            result = result.replaceAll("\\.", ",");
            result = result.replaceAll("-", ".");
        }
        return result.replaceAll(",", "");
    }
}
