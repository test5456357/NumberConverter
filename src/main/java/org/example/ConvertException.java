package org.example;

public class ConvertException extends Exception {
    private ConvertException(String msg) {
        super(msg);
    }

    public static ConvertException nullValue() {
        return new ConvertException("Initial value missing");
    };

    public static ConvertException numbersNotFound() {
        return new ConvertException("Numbers not found.");
    };

    public static ConvertException extraCurrencySymbols() {
        return new ConvertException("Extra currency symbols.");
    }

    public static ConvertException extraNegativitySymbols() {
        return new ConvertException("Extra negativity symbols.");
    }
    public static ConvertException unableToDetermineDelimiter() {
        return new ConvertException("Unable to determine delimiter.");
    }

    public static ConvertException thousandSeparatorsIncorrect() {
        return new ConvertException("Thousand separators are not correct.");
    }
    public static ConvertException containsInvalidCharacters() {
        return new ConvertException("Contains invalid characters.");
    }
}
