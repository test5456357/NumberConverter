package org.example;

import java.util.Scanner;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        Logger logger = Logger.getLogger(Main.class.getName());
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.print("Enter a string to convert (or 'exit' to exit): ");
            String input = scanner.nextLine();

            if ("exit".equalsIgnoreCase(input)) {
                System.out.println("Exit from the program.");
                break;
            }

            try {
                Double result = ConverterService.numberConverter(input);
                System.out.println("Result: " + result);
            } catch (NumberFormatException | ConvertException ex) {
                String logitem = String.format("Error: \"%s\"", ex.getMessage());
                logger.warning(logitem);
            }
        }

        scanner.close();
    }
}
